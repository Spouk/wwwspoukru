module wwwspoukru

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/jinzhu/gorm v1.9.16
	gitlab.com/Spouk/gotool v0.0.0-20210814190327-15c61b74cc1e
)
