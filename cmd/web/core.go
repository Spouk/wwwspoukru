package main

import (
	`io`
	`io/fs`
	`log`
	`os`
	`os/signal`
	`syscall`
	`time`
)

const logprefixcore = "[core]\t"
type (
	Core struct {
		Log    *log.Logger
		cfg    *Config
		sigs   chan os.Signal
		Server *server
	}
)

func NewCore(configfn  string) (*Core, error)  {
	//instance core
	c := &Core{
		Log:    log.New(os.Stdout,logprefixcore, log.LstdFlags |log.Lshortfile),
		cfg:    nil,
		sigs:   make(chan os.Signal,1),
		Server: nil,
	}

	//read config from file
	cfg, err := NewConfig(configfn)
	if err != nil {
		c.Log.Println(err)
		return nil, err
	}
	c.cfg = cfg

	//create/open append logfile
	fh, err := c.OpenLogFile(c.cfg.LogFile)
	if err != nil {
		c.Log.Println(err)
		return nil, err
	}

	//make mux logging
	logmux :=io.MultiWriter(os.Stdout, fh)
	c.Log.SetOutput(logmux)

	//set list sig* for catching
	signal.Notify(c.sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL, syscall.SIGSTOP)

	//run sig* catcher
	go c.catcherSigKiller()

	//make new server
	c.Server = NewServer(c)

	return c, nil
}

func (c *Core) catcherSigKiller() {
	//cathing interrupt, make need actions
	sig := <-c.sigs
	c.Log.Printf("\n\n[WARNING] пойман сигнал прерывания работы сервера `%v`,  что не есть хорошо!, подождите немного пока я корректно завершу работу, ок? я так и думал... \n\n", sig)

	//save sessions from memory to database
	//2 path: release middleware after middleware session extract session object
	//adding information/check user authorization and more actions and goto next context layer

	//working need for save open files/dump configs and more  actions
	time.Sleep(time.Second * 2)

	//exit
	os.Exit(1)
}

func (c *Core) OpenLogFile(filename string) (*os.File, error) {
	c.Log.Printf("filename log: %s\n", filename)
	if stat, err := os.Stat(filename); err != nil {
		if err == os.ErrNotExist {
			fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		} else {
			if _, ok := err.(*fs.PathError); ok {
				fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
				if err != nil {
					return nil, err
				}
				return fh, nil
			}
			return nil, err
		}
	} else {
		if !stat.IsDir() {
			fh, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		}
	}
	return nil, nil
}