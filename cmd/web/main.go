package main

import (
	`flag`
	`log`
	`os`
	`wwwspoukru/pkg/models/mysql`
)

func main() {
	//set flags for run application
	addr := flag.String("addr", ":2000", "address for run server")
	cfg := flag.String("cfg", "./cfg/config.yaml","config file")
	flag.Parse()
	log.Println(os.Args[1:], len(os.Args[1:]))
	if len(os.Args[1:]) != 4 || (addr == nil && cfg == nil) {
		flag.PrintDefaults()
		os.Exit(2)
	}
	log.Println(os.Args[1:], len(os.Args[1:]))

	//create core instance
	core, err := NewCore(*cfg)
	if err != nil {
		log.Fatal(err)
	}
	ms := mysql.Spoukrumodel{
		DB:  core.Server.db,
		Log: core.Log,
	}
	ms.Createtables()


	//run server
	core.Server.run(*addr)

}