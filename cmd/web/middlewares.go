package main

import (
	`fmt`
	`net/http`
	`time`
)

//*******************************************************************
// anti xss and clickjacking
//*******************************************************************
func (app *server) secureheaders(n http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-XSS-Protection","1; mode=block")
		w.Header().Set("X-Frame-Options","deny")
		n.ServeHTTP(w, r)
	})
}

//*******************************************************************
// wrapper middleware for logging multiout stdout && file file
//*******************************************************************
type exp struct {
	http.ResponseWriter
	statuscode int
	someresult string
}

func wrapper(w http.ResponseWriter) *exp {
	return &exp{
		ResponseWriter: w,
		statuscode:     http.StatusOK,
	}
}
func (w *exp) WriteHeader(statuscode int) {
	w.statuscode = statuscode
	w.ResponseWriter.WriteHeader(statuscode)
}
func (app *server) loggingwebmulti(n http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := time.Now()
		patchw := wrapper(w)
		n.ServeHTTP(patchw, r)
		str := fmt.Sprintf("%s\t%s\t%v\t%v\t%v",
			r.Method, r.RequestURI, r.RemoteAddr, patchw.statuscode, time.Since(s))
		app.log.Println(str)
	})
}
