package main

import (
	`fmt`
	`github.com/go-chi/chi/v5`
	_ "github.com/go-sql-driver/mysql"
	`github.com/jinzhu/gorm`
	"gitlab.com/Spouk/gotool/render"
	`log`
	`net/http`
	`time`
	`wwwspoukru/pkg/sessions`
)

const DNS = "%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local"

type server struct {
	mux    *chi.Mux
	log    *log.Logger
	config *Config
	render *render.Render
	db     *gorm.DB
	session *sessions.Sessions
}

func NewServer(c *Core) *server {
	app := &server{
		log:    c.Log,
		config: c.cfg,
	}

	//make render
	app.render = render.NewRender(app.config.Templatepath, app.config.Templatedebug, app.log, app.config.Teamplatedebugfatal)

	//create instanse sessions
	app.session = sessions.NewSessions(app.log, true, app.config.CookieName, app.config.CookieDomain)

	//init routes
	app.mux = app.routes()

	//init admin routes
	app.mux.Mount("/admin", app.routesadmin())

	//set static path route
	app.SetStaticRoute("/static", "./ui/static", app.config.CoreDebug, app.log)

	//open/connect database
	app.db = app.opendb()


	return app
}

//run server
func (app *server) run(addr string) {
	app.log.Fatal(http.ListenAndServe(addr, app.mux))
}

//open/connection database
func (app *server) opendb() *gorm.DB {
	dns := fmt.Sprintf(DNS, app.config.DBUser, app.config.DBPassword, app.config.DBHost, app.config.DBPort, app.config.DBDatabase)
	db, err := gorm.Open("mysql", dns)
	if err != nil {
		app.log.Fatal(err)
	}
	//set params sql driver
	db.DB().SetMaxOpenConns(app.config.DBSetMaxOpenConns)
	db.DB().SetMaxIdleConns(app.config.DBSetMaxIdleConns)
	db.DB().SetConnMaxLifetime(time.Duration(app.config.DBSetConnMaxLifetime))

	return db
}

//create table in database
//func (app *server) createtables(list map[string]interface{}) {
//	for k, x := range list {
//		if err := app.db.CreateTable(x).Error; err != nil {
//			er := err.(*mysql.MySQLError)
//			if errors.Is(err, er) {
//				if er.Number == 1050 {
//					app.log.Printf("warning: `%v`  table already exists ", k)
//					continue
//				} else {
//					log.Fatal(err)
//				}
//			}
//		}
//	}
//}
