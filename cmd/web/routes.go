package main

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	`log`
	"net/http"
	`os`
)

//admin
func (app *server) routesadmin() *chi.Mux {
	//make new mux
	mux := chi.NewMux()

	//adding to tree middlewares current mux
	mux.Use(app.session.SessionMiddleware)
	mux.Use(app.secureheaders)
	mux.Use(app.loggingwebmulti)
	mux.Use(middleware.StripSlashes)
	mux.Use(middleware.RedirectSlashes)
	mux.Use(middleware.Recoverer)

	//adding routes
	mux.HandleFunc("/", app.admin)

	//set not found handler
	mux.NotFound(app.notfound)

	return mux
}

//public
func (app *server) routes() *chi.Mux {
	//make new mux
	mux := chi.NewMux()

	//adding to tree middlewares current mux
	mux.Use(app.session.SessionMiddleware)
	mux.Use(app.secureheaders)
	mux.Use(app.loggingwebmulti)
	mux.Use(middleware.StripSlashes)
	mux.Use(middleware.RedirectSlashes)
	mux.Use(middleware.Recoverer)

	//adding routes
	mux.HandleFunc("/", app.home)

	//set not found handler
	mux.NotFound(app.notfound)

	return mux
}

//реализация интерфейса http.FileSystem
type (
	MainerFS struct {
		fs    http.FileSystem
		debug bool
		log   *log.Logger
	}
)

func Dir(path string, debug bool, logger *log.Logger) MainerFS {
	return MainerFS{fs: http.Dir(path), debug: debug, log: logger}
}

//реализация паттерна интерфейса FileSystem пакета http
func (m MainerFS) Open(name string) (http.File, error) {
	workDir, _ := os.Getwd()
	if m.debug {
		m.log.Printf("OPEN FILENAME: %v :`%v`\n", name, workDir)
	}
	f, err := m.fs.Open(name)
	if err != nil {
		if m.debug {
			m.log.Printf("[ERROR] %v\n", err)
		}
		return nil, err
	}
	fi, err := f.Stat()
	if err != nil {
		if m.debug {
			m.log.Printf("[ERROR] %v\n", err)
		}
		return nil, err
	}
	if fi.IsDir() {
		if m.debug {
			m.log.Printf("IS DIR FU REQUEST....\n")
		}
		return nil, os.ErrPermission
	}
	return f, nil
}
