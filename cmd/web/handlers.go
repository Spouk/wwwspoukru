package main

import (
	`github.com/go-chi/chi/v5`
	`log`
	`net/http`
	`os`
	`path/filepath`
)

//*******************************************************************
// public
//*******************************************************************
func (app *server) home(w http.ResponseWriter, r *http.Request)  {
	ses:=app.session.Get(r)
	if err := app.render.Render("index.tmpl", ses, w); err != nil {
		app.log.Println(err)
	}
}

func (app *server) notfound(w http.ResponseWriter, r *http.Request)  {
	if err := app.render.RenderCode(http.StatusNotFound, "notfound.tmpl", nil, w); err != nil {
		app.log.Println(err)
	}
}


//*******************************************************************
// admin
//*******************************************************************
func (app *server) admin(w http.ResponseWriter, r *http.Request)  {
	app.session.ShowCache()
	if err := app.render.Render("admin.tmpl", nil, w); err != nil {
		app.log.Println(err)
	}
}

//----------------------------------------------------------------------
// static files
//-----------------------------------------------------------------------
func (app *server) SetStaticRoute(routePrefix string, realPathToFiles string, debug bool, log *log.Logger) {
	app.mux.Route(routePrefix, func(root chi.Router) {
		workDir, _ := os.Getwd()
		filesDir := filepath.Join(workDir, realPathToFiles)
		if app.config.CoreDebug {
			app.log.Printf("FilesDIR: %v\n", filesDir)
			app.log.Printf("GETWD: %v\n", workDir)
		}
		fs := http.StripPrefix(routePrefix, http.FileServer(Dir(filesDir, debug, log)))
		root.Get("/*", func(w http.ResponseWriter, r *http.Request) {
			fs.ServeHTTP(w, r)
		})
	})
}


