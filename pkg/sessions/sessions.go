package sessions

import (
	`bytes`
	`context`
	`crypto/sha1`
	`encoding/gob`
	`fmt`
	`log`
	`math/rand`
	`net/http`
	`strings`
	`sync`
	`time`
)

//TODO: adding cashe sessions by map -> key = session
const (
	ErrorNotFoundSection = "session: not found section"
	ErrorNotFoundKey     = "session: not found key"
)

type Sessions struct {
	log              *log.Logger
	addinfo          bool
	cookname, domain string
	cache            *cache
	//extFuncload      func(cookid string) []byte
}
type cache struct {
	stock map[string]*Session
	sync.Mutex
}

func NewSessions(l *log.Logger, addinforequest bool, cookname, domain string) *Sessions {
	s := &Sessions{
		log:         l,
		addinfo:     addinforequest,
		cookname:    cookname,
		domain:      domain,
		//extFuncload: fu,
		cache: &cache{
			stock:   make(map[string]*Session),
			Mutex: sync.Mutex{},
		},
	}
	return s
}
func (s *Sessions) SessionMiddleware(n http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		news := newsession()
		if s.addinfo {
			news.PutText("ra", r.RemoteAddr)
			news.PutText("ua", r.UserAgent())
			news.PutText("m", r.Method)
			news.PutText("c", r.Cookies())
		}

		//get cookie from request and find old sessions in dbs/and/or/cashe
		cook, err := r.Cookie(s.cookname)
		s.log.Println("--- session: get cookie ", cook, err)
		if err == nil {
			//cook  found in request
			//find in cache by id from  cook.value
			s.cache.Lock()
			v, found := s.cache.stock[cook.Value]
			s.cache.Unlock()

			if found {
				s.log.Println("--- session: session found in cache, loading now... ", cook, err)
				if s.addinfo {
					v.PutText("ra", r.RemoteAddr)
					v.PutText("ua", r.UserAgent())
					v.PutText("m", r.Method)
					v.PutText("c", r.Cookies())
				}
				//update session object
				s.cache.Lock()
				s.cache.stock[cook.Value] = v
				s.cache.Unlock()
				news = v
			} else {
				//session not found in cache, make new session object
				id := s.NewSHA1Hash(32)
				nc := http.Cookie{
					Name:    s.cookname,
					Value:   id,
					Path:    "/",
					Domain:  s.domain,
					Expires: time.Now().Add(356 * 24 * time.Hour),
				}
				s.log.Println("--- session: new cookie ", nc, s.cookname)
				http.SetCookie(w, &nc)

				//save new session object
				s.cache.Lock()
				s.cache.stock[id] = news
				s.cache.Unlock()
			}
		} else {
			//cook found in cache session, load it
			//s.LoadSession(s.extFuncload(cook.Value))
			//cookie not found in requst, create new cookie
			id := s.NewSHA1Hash(32)
			nc := http.Cookie{
				Name:    s.cookname,
				Value:   id,
				Path:    "/",
				Domain:  s.domain,
				Expires: time.Now().Add(356 * 24 * time.Hour),
			}
			s.log.Println("--- session: new cookie ", nc, s.cookname)
			http.SetCookie(w, &nc)

			//adding new session to cache
			s.cache.Lock()
			s.cache.stock[id] = news
			s.cache.Unlock()
		}
		//updating current context request sesson object
		ctx := context.WithValue(r.Context(), "session", news)
		n.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (s *Sessions) ShowCache()  {
	s.cache.Lock()
	for k, v := range s.cache.stock {
		s.log.Println(k, v)
	}
	s.cache.Unlock()
}

func (s *Sessions) GetAllSessions() map[string]*Session {
	start := time.Now()
	s.cache.Lock()
	dd := make(map[string]*Session, len(s.cache.stock))
	for k, v := range   s.cache.stock {
		dd[k] = v
	}
	s.cache.Lock()
	s.log.Println("--time copy cash stock :", time.Since(start))
	return dd
}
func (s *Sessions) Get(r *http.Request) *Session {
	return r.Context().Value("session").(*Session)
}

type Session struct {
	Datatext map[string]interface{}
	Databin  map[string]interface{}
	cookieid string
}

func newsession() *Session {
	return &Session{
		Datatext: make(map[string]interface{}),
		Databin:  make(map[string]interface{}),
		cookieid: "",
	}
}

//session gob packed byte slice
func (d *Sessions) LoadSession(packed []byte) {
	var stock bytes.Buffer
	_, err := stock.Read(packed)
	if err != nil {
		d.log.Printf("[errror] wrong read packed from loadsession functon %v\n", err)
		return
	}
	var ses = newsession()
	dec := gob.NewDecoder(&stock)
	err = dec.Decode(ses)
	if err != nil {
		d.log.Printf("[errror] decode  from loadsession functon %v\n", err)
		return
	}
	d.cache.Lock()
	defer d.cache.Unlock()
	if len(strings.TrimSpace(ses.cookieid)) > 1 {
		d.cache.stock[ses.cookieid] = ses
		return
	}
	d.log.Printf("[errror] len cookied < 1   loadsession functon %v\n", ses.cookieid)
}

//session gob packed and send external channel
func (d *Sessions) UnloadSession(cookid string) []byte {
	d.cache.Lock()
	defer d.cache.Unlock()
	if v, found := d.cache.stock[cookid]; found {
		var stock bytes.Buffer
		enc := gob.NewEncoder(&stock)
		err := enc.Encode(v)
		if err != nil {
			d.log.Printf("[errror] wrong encode from unloadsession functon %v\n", err)
			return nil
		}
		return stock.Bytes()
	}
	d.log.Printf("[errror] not found session by cookid unloadsession functon %v\n", cookid)
	return nil
}

func (d *Session) PutText(key string, value interface{}) {
	d.Datatext[key] = value
}
func (d *Session) PutBin(key string, value interface{}) {
	d.Databin[key] = value
}
func (d *Session) GetText(key string) interface{} {
	if v, found := d.Datatext[key]; found {
		return v
	} else {
		return nil
	}
}
func (d *Session) GetBin(key string) interface{} {
	if v, found := d.Databin[key]; found {
		return v
	} else {
		return nil
	}
}

// NewSHA1Hash generates a new SHA1 hash based on
// a random number of characters.
func (d *Sessions) NewSHA1Hash(n ...int) string {
	noRandomCharacters := 32

	if len(n) > 0 {
		noRandomCharacters = n[0]
	}

	randString := d.RandomString(noRandomCharacters)

	hash := sha1.New()
	hash.Write([]byte(randString))
	bs := hash.Sum(nil)

	return fmt.Sprintf("%x", bs)
}

// RandomString generates a random string of n length
func (d *Sessions) RandomString(n int) string {
	var characterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = characterRunes[rand.Intn(len(characterRunes))]
	}
	return string(b)
}
