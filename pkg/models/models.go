package models

import (
	`github.com/jinzhu/gorm`
	`time`
)

type (

	//таблица с сессиями
	Sessions struct {
		gorm.Model
		Cook        string `sql:"type:varchar(190); unique"` //cook = ID
		Active      bool
		Sessionblob  []byte `sql:"type:mediumblob; "` //
		LastConnect time.Time
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}

	//базовый конфиг , все изменные варианты хранить в Redis
	ConfigTable struct {
		gorm.Model
		Active bool
		Dump   string `sql:"type:varchar(100000); "`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//конфиги, подписка, файлы
	Files struct {
		gorm.Model
		Name    string `sql:"type:varchar(190); unique"`
		Path    string `sql:"type:longtext"`
		Alt     string `sql:"type:longtext"`
		Title   string `sql:"type:longtext"`
		Size    int64
		Ext     string
		TagFile string `sql:"type:longtext"` //метка для файла
		//IDS
		UserID    uint //gorm.Model.ID
		PostID    uint //ID статьи //gorm.Model.ID
		ElementID uint //ID привязки к элементу интерфейса //gorm.Model.ID
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//roles
	Roles struct {
		gorm.Model
		Name string `sql:"type:varchar(200); unique"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//rights by role
	Accessbit struct {
		gorm.Model
		Roleid                          uint //role.id
		Read, Write, Delete, Update     bool
		Aread, Awrite, Adelete, Aupdate bool
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//logs
	Logs struct {
		gorm.Model
		Userid uint   //user.id
		Action string `sql:"type:longtext"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//для подсчета количества просмотров страница; Alias - post.alias
	Linkref struct {
		Userid uint
		Cookie string `sql:"type:varchar(200)"`
		Alias  string `sql:"type:varchar(200)"`
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//счетчики
	Counters struct {
		gorm.Model
		Alias string `sql:"type:varchar(200)"` //post.alias
		Views int    //counts viewers
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
	//пользователь тут хранить только основную информацию
	User struct {
		gorm.Model
		//table struct
		Login        string `sql:"type:varchar(200); unique"`
		Passwordhash string `sql:"type:varchar(500)"`
		Cookie       string `sql:"type:varchar(200)"`
		Connected    bool
		Logged       bool
		Roleid       uint //role.id
		OA2vkid      uint
		OA2googleid  uint
		OA2yandexid  uint
		Lastconnect  time.Time
		Remoteaddr   string `sql:"type:varchar(200)"`
		Useragent    string `sql:"type:varchar(200)"`
		Email        string `sql:"type:varchar(190); unique"`
		Avatar       uint   //image.ID картинки на лого ; file.ID = [UINT]
		Username     string
		//temp data struct
		Role string
		//элементы структуры для веб-формы
		Errors map[string]string `gorm:"-"` //для проверки формы
		Robot  bool              `gorm:"-"`
	}
	//статьи
	Post struct {
		//table
		gorm.Model
		Tagid         uint      //gorm.Model.ID
		Authorid      uint      //gorm.Model.ID
		Title         string    `sql:"type:LONGTEXT"`
		Alias         string    `sql:"type:varchar(190); unique"`
		Body          string    `sql:"type:LONGTEXT"`
		PreBody       string    `sql:"type:LONGTEXT"`
		MetaKeys      string    `sql:"type:LONGTEXT"`
		MetaDesc      string    `sql:"type:LONGTEXT"`
		MetaRobot     string    `sql:"type:LONGTEXT"`
		MetaTitle     string    `sql:"type:LONGTEXT"`
		FileImagePost uint      `sql:"type:int unsigned"` //FileImagePost string    //(ID) (path) файла-картинки, который используется для отображения в списке статей основной картинки
		LaterPost     bool      //флаг отложенной публикации
		LaterData     time.Time //дата когда осуществить публикацию:: time.unix:time.unix
		Active        bool      //активна ли публикация
		//in struct adding data
		Files         []Files           //список других картинок и файлов, связанных с этой публикацией
		Errors        map[string]string `gorm:"-"` //для проверки формы
		LaterTimeForm string            `gorm:"-"` // поле для формы input.Time
		LaterDataForm string            `gorm:"-"` //полe для формы input.Data
	}
	//метки=категории
	Tag struct {
		gorm.Model
		Name      string `sql:"type:varchar(200); unique"`
		Desc      string `sql:"type:varchar(500);"`
		Countpost int
		Errors    map[string]string `gorm:"-"` //для проверки формы
	}

	//рекламные блоки
	Advertise struct {
		gorm.Model
		Tag    string            `sql:"type:varchar(190); unique"` //уникальная метка, за эту метку функция будет вытаскивать блок
		Type   string            // yandex, google, other
		Data   string            //js+html+css код блока для вставки на странице
		Errors map[string]string `gorm:"-"` //для проверки формы
	}
)
