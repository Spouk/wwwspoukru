package  mysql

import (
	`errors`
	`github.com/go-sql-driver/mysql`
	`github.com/jinzhu/gorm`
	`log`
	`wwwspoukru/pkg/models`
)

const (
	CREATE= iota
	DELETE
	UPDATE
	GETID
	GETALL
)
const FailIndex = -1



var (
	ErrCommandNotFound = errors.New("errors: command not found")
	ErrRecordsNotFound = errors.New("errors: records not found")
	listTable = map[string]interface{}{
		"configtable":  models.ConfigTable{},
		"files":        models.Files{},
		"user":         models.User{},
		"post":         models.Post{},
		"sessiontable": models.Sessions{},
		"roles":        models.Roles{},
		"logs":         models.Logs{},
		"accessbit":    models.Accessbit{},
		"linkref":      models.Linkref{}}
	)


type Spoukrumodel struct {
	DB *gorm.DB
	Log *log.Logger
}

func (m *Spoukrumodel) Createtables()  {
	m.Log.Println("---dbs : ", m.DB)
	for k, x := range listTable {
		if err := m.DB.CreateTable(x).Error; err != nil {
			er := err.(*mysql.MySQLError)
			if errors.Is(err, er) {
				if er.Number == 1050 {
					m.Log.Printf("warning: `%v`  table already exists ", k)
					continue
				} else {
					m.Log.Fatal(err)
				}
			}
		}
	}
}

