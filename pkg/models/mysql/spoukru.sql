-- ----------------------------
-- structure wwwspoukru
-- ----------------------------
create table if not exists files
(
    id        integer       not null auto_increment primary key,
    created   timestamp     not null default now(),
    name      varchar(200)  not null,
    size      int                    default 0,
    path      varchar(1000) not null,
    alt       varchar(200),
    title     varchar(400),
    ext       varchar(40),
    tagfile   varchar(40),
    userid    int,
    postid    int,
    elementid int
);
create table if not exists sessions
(
    id          integer   not null auto_increment primary key,
    created     timestamp not null default now(),
    userid      int       not null,
    session     varchar(1000),
    lastconnect timestamp          default now(),
    cookie      varchar(100),
    active      bool               default false
);

create table if not exists users
(
    id           integer             not null auto_increment primary key,
    created      timestamp           not null default now(),
    login        varchar(200)        not null unique,
    passwordhash varchar(300),
    cookie       varchar(200)        not null,
    connected    bool                         default false,
    logged       bool                         default false,
    roleid       int                 not null,
    logid        int                 not null,
    oa2vkid      int,
    oa2googleid  int,
    oa2yandexid  int,
    lastconnect  timestamp,
    remoteaddr   varchar(200),
    useragent    varchar(1000),
    email        varchar(200) unique not null,
    avatar       int,
    username     varchar(200)
);

create table if not exists tags
(
    id        integer      not null auto_increment primary key,
    created   timestamp    not null default now(),
    name      varchar(200) not null unique,
    desc      varchar(1000),
    countpost int                   default 0
);
create table if not exists roles
(
    id      integer      not null auto_increment primary key,
    created timestamp    not null default now(),
    name    varchar(200) not null unique
);
create table if not exists logs
(
    id      integer   not null auto_increment primary key,
    created timestamp not null default now(),
    userid  int,
    action  text
);

create table if not exists accessbit
(
    id      integer   not null auto_increment primary key,
    created timestamp not null default now(),
    roleid  int       not null,
    uread   bool,
    uwrite  bool,
    uupdate bool,
    udelete bool,
    aread   bool,
    awrite  bool,
    aupdate bool,
    adelete bool
);
create table if not exists counters
(
    id      integer   not null auto_increment primary key,
    created timestamp not null default now(),
    alias   varchar(500),
    views   int                default 0
);

create table if not exists post
(
    id            integer   not null auto_increment primary key,
    created       timestamp not null default now(),
    title         varchar(300),
    authorid      int       not null,
    body          text,
    prebody       text,
    alias         varchar(300),
    tagid         int       not null,
    metakeys      text,
    metadesc      text,
    metatitle     text,
    metarobot     text,
    fileimagepost int,
    laterpost     bool,
    laterdata     timestamp,
    active        bool
);
create table if not exists advertise
(
    id      integer      not null auto_increment primary key,
    created timestamp    not null default now(),
    tag     varchar(1000) unique,
    type    varchar(200) not null,
    data    text
);
create table if not exists linkref
(
    id      integer   not null auto_increment primary key,
    created timestamp not null default now(),
    userid  int       not null,
    cookie  varchar(200),
    alias   varchar(500)
);




